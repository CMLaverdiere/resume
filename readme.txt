For the Fontin font:

Ubuntu:
  download here: http://www.exljbris.com/fontin.html
  place the .ttf files in ~/.fonts
  run: sudo fc-cache -fv
  change Fontin-Regular to just Fontin if necessary.

Arch:
  https://aur.archlinux.org/packages/ttf-exljbris/
