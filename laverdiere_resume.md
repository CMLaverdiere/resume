Personal Info
=============

Email Address: <CMLaverdiere@gmail.com>

Personal Site: [cmlaverdiere.net](http://www.cmlaverdiere.net)

Phone Number: 443-519-7211

Github Profile: [cmlaverdiere](https://github.com/cmlaverdiere)

Work Experience
===============

## Current

Intern at the Johns Hopkins University Applied Physics Laboratory

## Summer 2015

Asymmetric Operations Sector

- Lead performance analysis of large, multithreaded c++ codebase, and
  directed refactoring efforts based on profiling results.

- Integrated graph visualization tools to support satellite-based network
  topologies.

## Summer 2014

Asymmetric Operations Sector

- Worked on a network / packet capture analysis tool written in C++ and Perl,
  where I oversaw RTP and IGMP protocol implementations, and UDP flow analysis.

- Created computer vision software for Microsoft Kinect intrusion detection system.

- Installed and maintained team tools for Agile development on a Fedora Linux
  server.

## Summer 2013

Technical Services Department

- Developed an application for generating dynamic technical reports using
  .NET based reporting tools.

- Worked on a Java application for the TSD Work-order Tracking System.

- Organized continuous build and stress testing infrastructure using
  Jenkins, TestNG, and JMeter.

- Created several Oracle SQL database views and tables for internal use.

- Used front-end web libraries in designing application pages, powered by
  Bootstrap and JQuery.

Education
=========

May 2016 University of Maryland, Baltimore County (UMBC)

Bachelors of Science in Computer Science and Mathematics

Cumulative GPA: 3.95 / 4.0

Programming Skills
==================

Comfortable with: Python, C, C++, Haskell, Rust, Java, Ruby, Shell, JS.

Adequate in: Lisp, R, PHP, x86 Assembly, Perl, SQL.

Frameworks / Software: Scikit-learn, Git, OpenGL, Rails, Flask, MATLAB, LAMP, Android.

Hardware: Raspberry Pi and Arduino board experience.

Other: Linux enthusiast with 5+ years experience (Debian, Arch, Fedora).

Personal Projects
=================

## [CScribe](http://cmlaverdiere.net/2015/06/24/cscribe-intro.html)

A tool to assist in music transcription, written in C.

## [SLMobileMech](http://slmm.herokuapp.com/)

Ruby on Rails application for a small car mechanic business.

## [Gfxhub](http://gfxhub.herokuapp.com/)

Flask (Python) application to host my rendered computer graphics images.

## [Four-to-the-Fifth](http://userpages.umbc.edu/~chlaver1/Four-to-the-Fifth/about.html)

Multi-platform web game using the Quintus HTML5 engine.

Extracurricular Activities
==========================

## UMBC Cyber Defense Club:

  - Analysed server exploits using nmap, netcat, Metasploit software.
  - Matasano Crypto Challenges, TinyCTF, SmashTheStack, etc.

## Linux Users Group:

  - Planned setting up various campus events, install-fests.
  - Critiqued various Linux distributions, init-systems, package managers.
